def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  arr.reduce(0, :+)
end

def multiply(*args)
  args.reduce(:*)
end

def power(base, power)
  base**power
end

def factorial(num)
  return 1 if num == 0
  (1..num).reduce(:*)
end
