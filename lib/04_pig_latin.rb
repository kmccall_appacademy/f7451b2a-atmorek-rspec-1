def translate(words)
  translated_words = words.split.map do |word|
    pig_latin = translate_word(word)
    word == word.capitalize ? pig_latin.capitalize : pig_latin
  end

  translated_words.join(" ")
end


def translate_word(word)
  vowels = "aeiou"

  # If word begins with a vowel, return word + "ay"
  return word + "ay" if vowels.include?(word[0])

  # otherwise, find the index of the first vowel
  # which will be the start of the translated word
  # A "u" preceded by a "q" doesn't count as a vowel
  vowel_idx = nil

  word.chars.each_with_index do |ch, idx|
    next if vowel_idx || !vowels.include?(ch)

    vowel_idx = idx unless ch.downcase == 'u' && word[idx - 1].downcase == 'q'
  end

  word[vowel_idx..-1] + word[0...vowel_idx] + "ay"
end
