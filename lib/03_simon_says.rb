def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, times=2)
  output = []
  times.times { output << word }
  output.join(" ")
end

def start_of_word(word, letters)
  word[0, letters]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  little_words = ["and", "the", "over"]

  capitalized_words = string.split.map.with_index do |word, idx|
    if little_words.include?(word) && idx != 0
      word
    else
      word.capitalize
    end
  end

  capitalized_words.join(" ")
end
